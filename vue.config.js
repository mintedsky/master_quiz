module.exports = {
   productionSourceMap: false, // no source maps, speed up compile time
   publicPath: '', // removes leading slash from file paths in the index.html file
   css: {
        extract: true
   },
  transpileDependencies: [
    "vuetify"
  ]
}