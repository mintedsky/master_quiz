import Vue from 'vue'
import VueRouter from 'vue-router'
import Main from '../views/Main.vue'
import authorization  from '../services/authorization';

Vue.use(VueRouter)

const router = new VueRouter({
  routes : [
        {
            path: '/login',
            name: 'Login',
            component: () => import('../views/Login.vue')
        },    
        {
            path: '/',
            name: 'Main',
            component: Main
        },
        {
            path: '/question',
            name: 'Question',
            component: () => import('../views/Question.vue')
        },
        {
            path: '/add-question',
            name: 'QuestionTypeSelect',
            component: () => import('../views/Add_Question.vue')
        },
        {
            path: '/add-category',
            name: 'Category',
            component: () => import('../views/Category.vue')
        },
        {
            path: '/text2speech',
            name: 'Text2Speech',
            component: () => import('../views/Text2Speech.vue')
        },
        {
            // will match everything
            path: '*',
            name:'Login',
            component: () => import('../views/Login.vue')
        }
    ]
});

router.beforeEach((to, from, next) => {
    
    if (to.path !== '/login') {
        /****************************************/
        // if path is not login, check if token exists
        // if yes, send them to next page
        // if no, send to login

        // console.log('token',authorization.checkToken())

        // commenting out until we can get login working
        if (authorization.checkToken()) {
            next();
        } else {
            next('/login');
        }

        /****************************************/
    } else {
        // if user is going to login, clear localStorage first
        authorization.logout();
        next();
    }
});

export default router;
