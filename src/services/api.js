import axios from 'axios'
import authorization from './authorization'
// import storage from './storage'
// import router from '../router'
// import store from '../vuex/index';

axios.defaults.baseURL =  process.env.VUE_APP_SERVICE_URL;


// axios.interceptors.request.use(function(config){
//     if(storage.methods.checkToken()) {
//         config.headers['X-Access-Token'] = storage.methods.getToken();
//     }
//     return config;
// });

const api = {
    login(credentials) {
      return new Promise((resolve,reject) => {

            axios.post('login', credentials)
               .then(res => {
                   console.log(res);
                resolve(res.data);
               })
               .catch(res=> {
                reject(res);
            });
        })
    },
    get(path, params) {
        return new Promise((resolve, reject) => {

            let config = {
                    headers: { 
                        'Accept': 'application/json', 
                        'Content-Type' : 'application/json',
                        'Authorization' : 'Bearer ' +  authorization.getToken()
                     }
                }

            axios.get(path, { params: params }, config).then(res => {
                resolve(res.data);

            }).catch(res => {
                if (res.response.status === 401 || res.response.status === 451) {
					reject(res.response);
					// res.response.data.message -- error message
					// res.response.status -- error status

                } else {
                    reject(res.response);
                }

            });
        })
    },
    post(path, params) {
        return new Promise((resolve, reject) => {

            let config = {
                headers: { 
                    'Accept': 'application/json', 
                    'Content-Type' : 'application/json',
                    'Authorization' : 'Bearer ' +  authorization.getToken()
                 }
            }


            axios.post(path + `?timestamp=${ Math.round((new Date()).getTime() / 1000) }`, params, config).then(res => {

                resolve(res.data);

            }).catch(res => {
                if (res.response.status === 401 || res.response.status === 451) {
					reject(res.response);
             
                } else {
                    reject(res.response);
                }


            });
        })
    },
   async sendFile(path, formData, progressCallback) {

            //return new Promise((resolve, reject) => {

            try {

                let headers = { 
                    'Content-type' : 'multipart/form-data'
                }

                return await axios.post(path, formData, {
                    headers: headers,
                    onUploadProgress: progressEvent => {
                        if (progressCallback) {
                            let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                            progressCallback(percentCompleted);
                        }
                    }
                });
            
            } catch (error) {
                throw this.errorHandler(error);
            }

        // }
    }

}

export default api
