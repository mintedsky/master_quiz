

const authorization = {

    getToken() {
        if(this.checkToken()) {
            return localStorage.getItem('loginToken') 
        }
    },

    checkToken() {
        return localStorage.getItem('loginToken') ? true : false;
    },

    logout() {
        Object.keys(localStorage).forEach((keyString)=>{
            localStorage.removeItem(keyString);
        });
        return;
    }

}

export default authorization;
